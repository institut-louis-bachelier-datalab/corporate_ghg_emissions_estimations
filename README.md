# Corporate GHG Emissions Estimations

Estimate corporate GHG emissions using an open methodology based on state of the art literature.


<br/>


- [About the project 💡](#about)
    - [Pladifes](#pladifes)
    - [CGEE](#cgee)
- [Quickstart 🚀](#quickstart)
    - [Installation ](#installation)
    - [Access to base models performances](#start-to-estimate-your-impact-)
- [Contributing 🤝](#contributing-)
- [Contact 📝](#contact-)

# About this project

## Pladifes

Pladifes is a research program aiming at easing the research in green and sustainable finance, as well as traditional one. It does this by enabling the development of [Eurofidai](https://www.eurofidai.org/) databases (daily & high frequency data for academic research). Another lever is the Institut Louis Bachelier team, providing data creation services and acces to inovative [ESG databases](https://pladifes.institutlouisbachelier.org/data/). They also developped the [ESG data Cartography](https://www.institutlouisbachelier.org/en/esg-data-cartography/).

## Corporate GHG Emissions Estimations

**CGEE** (standing for corporate GHG emissions estimations) is a package that has been developped to help researchers and practitionners access emission data based on an open methodology, with an extensive coverage and based on state of the art methodology.  

Databases produced in the context of this project are available [here](https://pladifes.institutlouisbachelier.org/data/#ghg-estimations). 

This repository allow us to share our methodology, to help users understand how this data was created.  


# Quickstart 🚀

## Installation 

Installation procedure to come.

## Access to base models performances

Access to base models performances to come.

## Models customization

Please reach out to us if you want to discuss how to modify our models according to your project.


# Contributing 🤝

We are hoping that the open-source community will help us edit the code and make it better!

You are welcome to open issues, even suggest solutions and better still contribute the fix/improvement! We can guide you if you're not sure where to start but want to help us out 🥇

In order to contribute a change to our code base, you can submit a pull request (PR) via GitLab and someone from our team will go over it. Yet, we usually prefer that you reach out to us before doing so at [contact](mailto:pladifes@institutlouisbachelier.org). 


# Contact 📝

Maintainers are [Mohamed FAMAHOUI](https://www.linkedin.com/in/mohamed-fahmaoui-b30587176/) and [Thibaud BARREAU](https://www.linkedin.com/in/thibaud-barreau/). CGEE is developed in the context of the [Pladifes](https://pladifes.institutlouisbachelier.org/) project. If you use Pladifes repositories, models or datasets for academic research, you must mention the folowing phrase : "Work carried out using resources provided by the Equipex Pladifes (ANR-21-ESRE-0036), hosted by the Louis Bachelier Institute".
